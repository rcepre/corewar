####COREWAR 

![purpose](https://img.shields.io/badge/purpose-education-green.svg)
![purpose](https://img.shields.io/badge/42-School-Blue.svg)

*by rgermain, loiberti, rcepre*

Le Corewar est un jeu très particulier.
Il consiste à rassembler autour d’une "machine virtuelle" des "joueurs",
lesquels vont y charger des "champions" qui vont se battre à l’aide de processus,
dans le but, entre autres, de faire en sorte qu’on dise d'eux qu’ils sont "en vie".
Les processus s’exécutent séquentiellement au sein de la même machine virtuelle
et du même espace mémoire. Ils peuvent donc, entre autre, s’écrire les uns sur 
les autres afin de se corrompre mutuellement, de forcer les autres à 
exécuter des instructions qui leur font du mal, de tenter de recréer à la volée 
l’équivalent logiciel d’un Côtes du Rhône 1982, etc ...
Le jeu se termine quand plus aucun processus n’est en vie. 
À ce moment là, le gagnant est le dernier joueur à avoir été rapporté comme étant
"en vie".

##Découpage du projet 


1.  L’assembleur: C’est le programme qui va compiler vos champions et les
    traduire du langage dans lequel vous allez les écrire (l’assembleur) 
    vers un "bytecode", à savoir un code machine qui sera directement interprété
    par la machine virtuelle.
2.  La machine virtuelle: C’est l’"arène" dans laquelle les champions vont 
    s’exécuter. 
3.  Le champion: il est ecrit en langage assembleur, et est compileé par le 
    programme asm, puis peut etre lanceé par la VM de corewar.
4. Notre projet comprend aussi une désassembleur. 

**Le visualisateur est aussi sonore.**

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/J5r7GInOLNQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Le visualisateur en fullscreen (80's mode), avec un champion chargé: 

![](https://gitlab.com/rcepre/corewar/raw/master/pictures/cw.png)

Sans fullscreen avec un peu plus de champions: 

![](https://gitlab.com/rcepre/corewar/raw/master/pictures/cw3.png)

![](https://gitlab.com/rcepre/corewar/raw/master/pictures/cw4.png)

Gestion des erreurs clang-like de l'asm, et mode verbose: 

![](https://gitlab.com/rcepre/corewar/raw/master/pictures/asm_pict.png)

Verbose de la VM: 

![](https://gitlab.com/rcepre/corewar/raw/master/pictures/verbsoe.png)
